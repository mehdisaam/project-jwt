import React,{useEffect, useState} from 'react'
import {connect} from 'react-redux'
import {fetchUser} from '../../redux/actions/userActions'
import {useDispatch} from 'react-redux'
import './login.css'

const LoginComponent = ()=> {
    const dispatch = useDispatch()
    const [username,setUsername]= useState("")
    const [password,setPassword]= useState("")
      const  handleOnChangeUser = (e) => {
            e.persist();
            setUsername(e.target.value)
        }
        const  handleOnChangePassword = (e) => {
            e.persist();
            setPassword(e.target.value)
        }
    
   const onSubmit = (e) => {
        e.preventDefault()
        dispatch(fetchUser(username,password))
    }
useEffect(()=>{
    dispatch(fetchUser())
})
   
        return(
            <div className="main-sign_input">
                <h1>Login Form</h1>
                <form onSubmit={onSubmit}>
                    <input 
                        type="text" 
                        name="username" 
                        placeholder="Username" 
                        value={username}
                        onChange={handleOnChangeUser}
                    />
                    <br/>
                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={password}
                        onChange={handleOnChangePassword}
                    />
                    <br/>
                    <input
                        type="submit"
                        value="Login"
                        className='main-sign_submit'
                    />
                </form>
            </div>
        )
    
}


export default LoginComponent