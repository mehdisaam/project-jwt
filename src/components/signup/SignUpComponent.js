import React, { useEffect, useState } from 'react'
import { signUserUp } from '../../redux/actions/userActions'
import { useDispatch,useSelector } from 'react-redux'
import './signup.css'
const SignUpComponent = () => {
    const dispatch = useDispatch()
    const userReducer = useSelector(state=>state.userReducer.user)
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [age, setAge] = useState("")
    const handleOnChangeUser = (e) => {
        setUsername(e.target.value)
    }
    const handleOnChangePassword = (e) => {
        setPassword(e.target.value)
    }

    const handleOnChangeAge = (e) => {
        setAge(e.target.value)
    }

    const onSubmit = (e) => {
        e.preventDefault()
        dispatch(signUserUp(username,password))
        console.log(password,userReducer)
    }

    useEffect(() => {
        dispatch(signUserUp())
    },[])

    return (
        <div className="main-sign_input">
            <h1>SignUp Form</h1>
            <form onSubmit={onSubmit}>
                <input
                    type="text"
                    name="username"
                    placeholder="Username"
                    value={username}
                    onChange={handleOnChangeUser}
                />
                <br />
                <input
                    type="password"
                    name="password"
                    placeholder="Password"
                    value={password}
                    onChange={handleOnChangePassword}
                />
                <br />
                <input
                    type="number"
                    name="age"
                    placeholder="Age"
                    value={age}
                    onChange={handleOnChangeAge}
                />

                <br />
                <input
                    type="submit"
                    value="signUp"
                    className="main-sign_submit"
                    
                />
            </form>
        </div>
    )

}


export default SignUpComponent