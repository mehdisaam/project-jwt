import userReducer from './userReducer'
import currentReducer from './currentReducer'
import {combineReducers} from 'redux'

const rootReducer = combineReducers({
    userReducer,
    currentReducer
})

export default rootReducer